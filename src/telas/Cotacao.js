import React, { useEffect, useState } from "react";
import { Text, StyleSheet, Image, Dimensions, View } from "react-native";

import conversor from "../../assets/conversor.png";
import SelectDropdown from "react-native-select-dropdown";

const width = Dimensions.get("screen").width;

export default function Cotacao({ navigation }) {
  const [moedas, setMoedas] = useState([]);
  useEffect(() => {
    const lerMoedas = async () => {
      try {
        const moedasHTTP = await fetch(
          "https://www.frankfurter.app/currencies"
        );
        const moedasJson = await moedasHTTP.json();
        setMoedas(moedasJson);
      } catch (error) {
        console.log(error);
      }
    };
    lerMoedas();
  }, []);

  var moedasTexto = Object.values(moedas);
  var moedasSimbolo = Object.keys(moedas);
  let simboloMoedaLocal = "";
  let simboloMoedaEstrangeira = "";

  return (
    <>
      <Image source={conversor} style={estilos.conversor} />
      <View>
        <Text style={estilos.titulo}>Cotação</Text>
        <View style={estilos.moeda}>
          <Text style={estilos.tituloMoedaLocal}>Moeda Local</Text>
          <SelectDropdown
            data={moedasTexto}
            defaultButtonText="Selecione a opção"
            onSelect={(selectedItem, index) => {
              moedasSimbolo.forEach(function (simbolo, idx) {
                if (idx == index) {
                  simboloMoedaLocal = simbolo;
                }
              });
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
          />
        </View>
        <View style={estilos.moeda}>
          <Text style={estilos.tituloMoeda}>Moeda estrangeira</Text>
          <SelectDropdown
            data={moedasTexto}
            defaultButtonText="Selecione a opção"
            onSelect={(selectedItem, index) => {
              moedasSimbolo.forEach(function (simbolo, idx) {
                if (idx == index) {
                  simboloMoedaEstrangeira = simbolo;
                }
              });
              if (simboloMoedaLocal != "" && simboloMoedaEstrangeira != "") {
                navigation.navigate("Conversao", {
                  sblLocal: simboloMoedaLocal,
                  sblEstrangeira: simboloMoedaEstrangeira,
                });
              }
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
          />
        </View>
      </View>
    </>
  );
}

const estilos = StyleSheet.create({
  conversor: {
    width: "100%",
    height: (276 / 274) * width,
  },
  titulo: {
    width: "100%",
    position: "relative",
    textAlign: "center",
    fontSize: 26,
    lineHeight: 26,
    fontWeight: "bold",
    padding: 16,
  },
  moeda: {
    flexDirection: "row",
    paddingVertical: 16,
    padding: 16,
  },
  tituloMoedaLocal: {
    fontSize: 16,
    lineHeight: 26,
    marginLeft: 12,
    paddingRight: 60,
  },
  tituloMoeda: {
    fontSize: 16,
    lineHeight: 26,
    marginLeft: 12,
    paddingRight: 16,
  },
});
