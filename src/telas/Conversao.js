import React, { useEffect, useState } from "react";
import {
  Text,
  Button,
  View,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  TextInput,
} from "react-native";

import conversor from "../../assets/conversor.png";

const width = Dimensions.get("screen").width;

export default function Conversao({ navigation, route }) {

  const sblLocal = route.params.sblLocal;
  const sblEstrangeira = route.params.sblEstrangeira;

  const [number, setNumber] = useState("1");
  const [resultado, setResultado] = useState([]);
  const [resultadoTexto, setResultadoTexto] = useState(" ");
  
  useEffect(() => {
    setNumber(number);
    lerResultado()
  }, [number, resultadoTexto]);



  console.log(number, resultado)
 
  const lerResultado = async () => {
     try {
      let resultadoHTTP = await fetch(
        `https://www.frankfurter.app/latest?amount=${number}&from=${sblLocal}&to=${sblEstrangeira}`
      );
      let resultadoJson = await resultadoHTTP.json();
      setResultado(resultadoJson);
      setResultadoTexto(`${sblLocal}$ ${number} valem ${sblEstrangeira}$ ${Object.values(resultado.rates || " ")}`);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Image source={conversor} style={estilos.conversor} />
      <SafeAreaView>
        <View>
          <Text style={estilos.titulo}>Converter</Text>
          <TextInput
            style={estilos.input}
            onChangeText={setNumber}
            value={number}
          />
          <Text style={estilos.texto}>
            {resultadoTexto}
          </Text>
        </View>
      </SafeAreaView>
    </>
  );
}

const estilos = StyleSheet.create({
  conversor: {
    width: "100%",
    height: (276 / 274) * width,
  },
  titulo: {
    width: "100%",
    position: "relative",
    textAlign: "center",
    fontSize: 26,
    lineHeight: 26,
    fontWeight: "bold",
    padding: 16,
  },
  texto: {
    fontSize: 20,
    lineHeight: 26,
    fontWeight: "bold",
    padding: 16,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
