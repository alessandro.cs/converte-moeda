import { StatusBar, SafeAreaView } from 'react-native';
import Conversao from './src/telas/Conversao';
import Cotacao from './src/telas/Cotacao';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default function App({}) {
  return (
    <NavigationContainer>
      <StatusBar/>
      <Stack.Navigator>
        <Stack.Screen
          name="Cotacao"
          component={Cotacao}
          options={{ title: 'Bem-Vindo!' }}        
        />
        <Stack.Screen name="Conversao" component={Conversao}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

